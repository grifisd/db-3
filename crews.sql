--у нас есть города
CREATE TABLE cities (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  name VARCHAR(15) NOT NULL,

  PRIMARY KEY (id)
);

--в городах работают рабочие
CREATE TABLE workers (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  surname VARCHAR(15) NOT NULL,
  city_id NUMBER NOT NULL,

  PRIMARY KEY (id),

  CONSTRAINT fk_workers
    FOREIGN KEY (city_id)
    REFERENCES cities(id)
);

--есть бригады: id + имя
CREATE TABLE crews (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  name VARCHAR(15) NOT NULL,

  PRIMARY KEY(id)
);

--иногда они (рабочие) обьединяются в команды с должностями
CREATE TABLE crews_lineup (
  crew_id NUMBER NOT NULL,
  position VARCHAR(30) NOT NULL,
  worker_id NUMBER NOT NULL,

  PRIMARY KEY(crew_id, position),

  CONSTRAINT fk_crews
    FOREIGN KEY (crew_id)
    REFERENCES crews(id),

    FOREIGN KEY (worker_id)
    REFERENCES workers(id)
);

--есть станции в конкретных городах, в которых живут рабочие и команды
CREATE TABLE stations (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  city_id NUMBER NOT NULL,
  name VARCHAR(30) NOT NULL,

  PRIMARY KEY(id),

  CONSTRAINT fk_stations
    FOREIGN KEY (city_id)
    REFERENCES cities(id)
);