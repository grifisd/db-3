--inserting data

--CITIES
INSERT INTO CITIES (name)
  VALUES ('Moscow');
INSERT INTO CITIES (name)
  VALUES ('Novosibirsk');
INSERT INTO CITIES (name)
  VALUES ('St. Petersburg');

--WORKERS
INSERT INTO WORKERS (surname, city_id)
  VALUES ('Ivanow', 1);
INSERT INTO WORKERS (surname, city_id)
  VALUES ('Petrow', 1);
INSERT INTO WORKERS (surname, city_id)
  VALUES ('Sidorow', 1);
INSERT INTO WORKERS (surname, city_id)
  VALUES ('Smirnow', 2);
INSERT INTO WORKERS (surname, city_id)
  VALUES ('Kuznecov', 2);
INSERT INTO WORKERS (surname, city_id)
  VALUES ('Popow', 3);
INSERT INTO WORKERS (surname, city_id)
  VALUES ('Sokolov', 3);

--CREWS
INSERT INTO CREWS (name)
  VALUES ('Banda')
INSERT INTO CREWS (name)
  VALUES ('Brigade')
INSERT INTO CREWS (name)
  VALUES ('DreamTeam')

--CREWS_LINEUP
INSERT INTO CREWS_LINEUP
  VALUES (1, 'Driver', 1);
INSERT INTO CREWS_LINEUP
  VALUES (1, 'Conductor', 2);
INSERT INTO CREWS_LINEUP
  VALUES (1, 'Policeman', 3);
INSERT INTO CREWS_LINEUP
  VALUES (2, 'Driver', 4);
INSERT INTO CREWS_LINEUP
  VALUES (2, 'Conductor', 5);
INSERT INTO CREWS_LINEUP
  VALUES (3, 'Driver', 21);
INSERT INTO CREWS_LINEUP
  VALUES (3, 'Conductor', 22);

--STATIONS
INSERT INTO stations (city_id, name)
  VALUES (1, 'Moscow-Rizhskyi');
INSERT INTO stations (city_id, name)
  VALUES (1, 'Moscow-Paveleckyi');
INSERT INTO stations (city_id, name)
  VALUES (2, 'Novosibirsk-Glavnyi');
INSERT INTO stations (city_id, name)
  VALUES (2, 'Novosibirsk-Rechnoy_vokzal');
INSERT INTO stations (city_id, name)
  VALUES (2, 'Novosibirsk-Zapadnyi');
INSERT INTO stations (city_id, name)
  VALUES (3, 'Peterburg-Glavnyi');

--CATEGORIES
INSERT INTO categories (name)
  VALUES ('generic');
INSERT INTO categories (name)
  VALUES ('econom');
INSERT INTO categories (name)
  VALUES ('coupe');
INSERT INTO categories (name)
  VALUES ('luxury');

--TRAINS
INSERT INTO trains (name)
  VALUES ('m-157');
INSERT INTO trains (name)
  VALUES ('m-158');
INSERT INTO trains (name)
  VALUES ('m-159');
INSERT INTO trains (name)
  VALUES ('m-171');
INSERT INTO trains (name)
  VALUES ('m-177');
INSERT INTO trains (name)
  VALUES ('n-225');
INSERT INTO trains (name)
  VALUES ('i-226');
INSERT INTO trains (name)
  VALUES ('i-1');
INSERT INTO trains (name)
  VALUES ('n-777');

--SCHEDULES
INSERT INTO SCHEDULES (name)
  VALUES ('sched-1');
INSERT INTO SCHEDULES (name)
  VALUES ('sched-2');
INSERT INTO SCHEDULES (name)
  VALUES ('sched-3');
INSERT INTO SCHEDULES (name)
  VALUES ('sched-4');
INSERT INTO SCHEDULES (name)
  VALUES ('sched-5');

--WAY_STATIONS:
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (1, 1, TIMESTAMP '1997-01-31 09:26:50', 15)
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (1, 2, TIMESTAMP '1997-02-01 04:17:50', 30)
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (1, 3, TIMESTAMP '1997-01-30 09:26:50', 5)
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (1, 4, TIMESTAMP '1997-05-31 09:26:50', 60)

INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (2, 6, TIMESTAMP '1997-06-30 09:26:50', 0)
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (2, 4, TIMESTAMP '1997-07-30 09:26:50', 15)

INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (3, 5, TIMESTAMP '1997-05-31 09:26:50', 15)
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (3, 6, TIMESTAMP '1997-01-31 09:26:50', 15)

INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (21, 3, TIMESTAMP '1991-01-31 02:26:50', 15)
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (21, 2, TIMESTAMP '1991-01-31 01:26:50', 15)
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (21, 1, TIMESTAMP '1991-01-31 03:26:50', 15)

INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (22, 5, TIMESTAMP '1992-04-28 09:26:50', 15)
INSERT INTO WAY_STATIONS (schedule_id, station_id, arrival_date, park_time)
  VALUES (22, 1, TIMESTAMP '1992-04-29 09:26:50', 15)

--TRIPS:
INSERT INTO TRIPS (schedule_id, train_id, crew_id, real_departure_date)
  VALUES (1, 1, 1, TIMESTAMP '2018-12-12 09:00:00')
INSERT INTO TRIPS (schedule_id, train_id, crew_id, real_departure_date)
  VALUES (1, 2, 2, TIMESTAMP '2018-12-13 09:00:00')
INSERT INTO TRIPS (schedule_id, train_id, crew_id, real_departure_date)
  VALUES (2, 3, 3, TIMESTAMP '2018-12-11 15:00:00')
INSERT INTO TRIPS (schedule_id, train_id, crew_id, real_departure_date)
  VALUES (2, 2, 3, TIMESTAMP '2018-12-10 15:00:00')
INSERT INTO TRIPS (schedule_id, train_id, crew_id, real_departure_date)
  VALUES (3, 5, 1, TIMESTAMP '2018-11-12 09:00:00')
INSERT INTO TRIPS (schedule_id, train_id, crew_id, real_departure_date)
  VALUES (21, 4, 2, TIMESTAMP '2018-10-12 09:00:00')
INSERT INTO TRIPS (schedule_id, train_id, crew_id, real_departure_date)
  VALUES (22, 6, 1, TIMESTAMP '2018-12-15 09:00:00')

--TICKETS_OLD:
INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (1, 1, 1, 10);
INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (1, 2, 2, 8);
INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (1, 3, 21, 5);
INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (1, 1, 22, 1);

INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (2, 1, 1, 5);
INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (2, 1, 2, 13);
INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (2, 21, 22, 100);
INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (2, 1, 21, 10);

INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (3, 22, 1, 10);
INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (3, 23, 2, 8);

INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (4, 26, 21, 5);

INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (5, 27, 1, 99);

INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (21, 22, 22, 212);

INSERT INTO TICKETS (trip_id, way_station_id, category_id, count)
  VALUES (22, 4, 22, 1);

--TICKETS_NEW:
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (1, 1, 2, 1, NULL);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (1, 1, 2, 1, NULL);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (1, 1, 2, 1, 1);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (1, 1, 2, 2, NULL);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (1, 1, 3, 21, NULL);

INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (2, 1, 2, 1, NULL);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (2, 2, 3, 1, NULL);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (2, 1, 21, 1, 3);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (2, 21, 2, 2, 3);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (2, 1, 3, 21, NULL);

INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (3, 22, 23, 1, NULL);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (3, 22, 22, 2, NULL);

INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (4, 26, 5, 1, NULL);
INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (4, 26, 25, 21, 1);

INSERT INTO TICKETS (trip_id, src_way_station_id, dst_way_station_id, category_id, passenger_trip_id)
  VALUES (5, 27, 28, 1, NULL);

--DELAYS:
INSERT INTO DELAYS (trip_id, way_station_id, delay)
  VALUES (1, 1, 11)
INSERT INTO DELAYS (trip_id, way_station_id, delay)
  VALUES (1, 3, 13)
INSERT INTO DELAYS (trip_id, way_station_id, delay)
  VALUES (5, 27, 19)