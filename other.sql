--есть разные удобства по разным ценам: от третьей полки до лакшери СВ
CREATE TABLE categories (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  name VARCHAR(15) NOT NULL,

  PRIMARY KEY(id)
);

--есть поезда: название
CREATE TABLE trains (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  name VARCHAR(10) NOT NULL,

  PRIMARY KEY (id)
);

--есть общее постоянное расписание: id, время и станция отправления, время (день недели + время суток) и станция прибытия
--возможно стоит убрать время и станция отправления/прибытия и занести их в way_stations, чтобы в tickets проще вносить билеты.
--но тогда непонятен смысл таблицы schedules
--сейчас удалим, потом можно будет добавить//удалить через ALTER TABLE
/*CREATE TABLE schedules (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  src_station_id NUMBER NOT NULL,
  dst_station_id NUMBER NOT NULL,

  departure_date DATE NOT NULL,
  arrival_date DATE NOT NULL,

  PRIMARY KEY(id),

  CONSTRAINT fk_schedules
    FOREIGN KEY (src_station_id),
    REFERENCES stations(id)

    FOREIGN KEY (dst_station_id)
    REFERENCES stations(id)
);*/
CREATE TABLE schedules (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  name VARCHAR(15) NOT NULL,

  PRIMARY KEY(id)
);

--есть все билеты на конкретные РЕЙСЫ С конкретной промежуточной станции ДО конкретной промежуточной станции
--с номером поездки пассажира: если null, то этот билет еще никто не купил
--изменять это состояние - через update
CREATE TABLE tickets (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  trip_id NUMBER NOT NULL,
  src_way_station_id NUMBER NOT NULL,
  dst_way_station_id NUMBER NOT NULL,
  category_id NUMBER NOT NULL,
  passenger_trip_id NUMBER,

  PRIMARY KEY(id),

  CONSTRAINT fk_tickets
    FOREIGN KEY (trip_id)
    REFERENCES trips(id),

    FOREIGN KEY (src_way_station_id)
    REFERENCES way_stations(id),

    FOREIGN KEY (dst_way_station_id)
    REFERENCES way_stations(id),

    FOREIGN KEY (category_id)
    REFERENCES categories(id)
);

--есть задержки для данного РЕЙСА на данной промежуточной станции (в минутах)
CREATE TABLE delays (
  trip_id NUMBER NOT NULL,
  way_station_id NUMBER NOT NULL,
  delay NUMBER,

  PRIMARY KEY (trip_id, way_station_id),

  CONSTRAINT fk_delays
    FOREIGN KEY (trip_id)
    REFERENCES trips(id),

    FOREIGN KEY (way_station_id)
    REFERENCES way_stations(id)
);
