1) Величина задержки указанного поезда в указанное время.

SELECT id, train_id, real_departure_date, sum(delay) from trips
  INNER JOIN delays ON delays.trip_id = trips.id
WHERE train_id = 1 AND real_departure_date BETWEEN DATE '2018-12-10' AND DATE '2018-12-14'
GROUP BY id, train_id, real_departure_date

2) Справка о маршрутах и поездах между указанными городами (без привязки к датам/расписанию).

SELECT station_id, name, schedule_id, arrival_date from way_stations
  INNER JOIN stations ON stations.id = way_stations.station_id
WHERE (stations.city_id = 1 OR stations.city_id = 2)
ORDER BY schedule_id, arrival_date

3) Станции-пересадки по маршруту до указанной станции (от заданной станции до заданной станции).

SELECT dst_way_station_id, name, passenger_trip_id, arrival_date FROM (
    SELECT * from trips
      INNER JOIN tickets ON tickets.trip_id = trips.id
      INNER JOIN way_stations ON way_stations.id = tickets.src_way_station_id
      INNER JOIN stations ON stations.id = way_stations.station_id
    WHERE (stations.id = 1 OR stations.id = 2) AND tickets.passenger_trip_id = 3
    ORDER BY passenger_trip_id, trips.schedule_id, way_stations.arrival_date
) WHERE arrival_date <> (
    SELECT MAX(arrival_date) from (
        SELECT way_stations.arrival_date from way_stations
          INNER JOIN tickets ON way_stations.id = tickets.src_way_station_id
        WHERE tickets.passenger_trip_id = 3
    )
)

4) Количество билетов на указанный поезд (от заданного города до заданного, в указанный промежуток времени) с заданным типом мест (плацкарт/купе/СВ).

SELECT
  trains.name as train,
  categories.name as category,
  count(tickets.id) as count from trips
  INNER JOIN tickets ON tickets.trip_id = trips.id
  INNER JOIN way_stations ON way_stations.schedule_id = trips.schedule_id
  INNER JOIN stations ON stations.id = way_stations.station_id
  INNER JOIN cities ON cities.id = stations.city_id
  INNER JOIN trains ON trains.id = trips.train_id
  INNER JOIN categories ON categories.id = tickets.category_id
WHERE train_id = 1
  AND (cities.id = 3 OR cities.id = 2)
  AND trips.real_departure_date BETWEEN DATE '2018-12-10' AND DATE '2018-12-14'
  AND tickets.passenger_trip_id IS NULL
GROUP BY trains.name, categories.name

5) Справка о ближайших поездах в указанный город в указанный отрезок времени.

SELECT
  cities.name as city,
  trips.real_departure_date as departure,
  trains.name as train FROM trips
  INNER JOIN way_stations ON way_stations.schedule_id = trips.schedule_id
  INNER JOIN stations ON stations.id = way_stations.station_id
  INNER JOIN cities ON cities.id = stations.city_id
  INNER JOIN trains ON trains.id = trips.train_id
WHERE real_departure_date BETWEEN DATE '2018-12-10' AND DATE '2018-12-14'
  AND city_id = 2