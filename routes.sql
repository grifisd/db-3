--есть промежуточные станции - для каждого расписания: id записи, расписание, станция остановки, время прибытия и время остановки (в минутах)
CREATE TABLE way_stations (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  schedule_id NUMBER NOT NULL,
  station_id NUMBER NOT NULL,
  arrival_date TIMESTAMP NOT NULL,
  park_time NUMBER NOT NULL,

  PRIMARY KEY(id),

  CONSTRAINT fk_way_stations
    FOREIGN KEY (schedule_id)
    REFERENCES schedules(id),

    FOREIGN KEY (station_id)
    REFERENCES stations(id)
);

--есть все РЕЙСЫ - конкретные расписания с конкретными поездами c конкретными бригадами в конкретную реальную дату
CREATE TABLE trips (
  id NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1),
  schedule_id NUMBER NOT NULL,
  train_id NUMBER NOT NULL,
  crew_id NUMBER NOT NULL,
  real_departure_date DATE NOT NULL,

  PRIMARY KEY(id),

  CONSTRAINT fk_trips
    FOREIGN KEY (train_id)
    REFERENCES trains(id),

    FOREIGN KEY (schedule_id)
    REFERENCES schedules(id),

    FOREIGN KEY (crew_id)
    REFERENCES crews(id)
);